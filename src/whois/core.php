<?php

namespace whois;

class core
{

    /**
     * Server Setting
     *
     * @var boolean
     */
    protected $server = false;

    /**
     * Whois Port
     *
     * @var integer
     */
    protected $port = 43;

    /**
     * Timeout before the lookup fails.
     *
     * @var integer
     */
    protected $timeout = 20;

    /**
     * Error Tracking
     *
     * @var boolean
     */
    protected $errorNumber = false;

    /**
     * Error Message
     *
     * @var boolean
     */
    protected $errorString = false;

    /**
     * Our Whois Query Paramaters.
     *
     * @var boolean
     */
    protected $query = false;

    /**
     * Data returned from whois lookup in raw form.
     *
     * @var boolean
     */
    protected $data = false;

    /**
     * Get the data from the whois service.
     *
     * @return this
     */
    public function get(){

        $this->data = null;

        // Creating a socket to connect with and launching it.
        $socket = @fsockopen(
                             $this->server, $this->port,
                             $this->errorNumber, $this->errorString,
                             $this->timeout
                            )
        or die("Error: " . $con['server'] . " stated " . $con['errorString'] . ".");

        fputs($socket, $this->query . "\r\n");

        // While... data... input result.
        while(!feof($socket)){

            // Append
            $this->data  .= fgets($socket);
        }

        // Close connection
        fclose($socket);

        // return
        return $this;

    }

    /**
     * Set Server
     *
     * @param string $server
     * @return this
     */
    public function setServer(string $server){

        $this->server = $server;

        return $this;
    }

    /**
     * Set our Query paramater.
     *
     * @param string $query
     * @return this
     */
    public function setQuery(string $query){

        $this->query = $query;

        return $this;

    }

    /**
     * Set our timeout.
     *
     * @param integer $timeout
     * @return this
     */
    public function setTimeout(int $timeout){

        $this->timeout = $timeout;

        return $this;

    }


    /**
     * Set the whois port.
     *
     * @param integer $port
     * @return this
     */
    public function setPort(int $port){

        $this->port = $port;

        return $this;
    }

    /**
     * Return Errors.
     *
     * @return array
     */
    public function error(){

        if($this->errorNumber === false && $this->errorString === false){

            return false;

        }

        return array('number' => $this->errorNumber, 'string' => $this->errorString);

    }

    /**
     * Return Data.
     *
     * @return string
     */
    public function data(){

        return $this->data;

    }

}