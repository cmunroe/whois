<?php

namespace whois;


/**
 * A Whois Class Built specificall for Team Cymru
 * 
 * $response = $whois->ip('192.168.1.1')->get()->clean();
 */
class cymru extends core
{

    // $response = $whois->ip('192.168.1.1')->get()->clean();

    /**
     * Setup for an IP Lookup.
     *
     * @param string $ip
     * @return this
     */
    public function ip(string $ip){

        $this->setServer("whois.cymru.com");
        
        $this->setQuery("-v " . $ip);

        return $this;
    }

    /**
     * Clean up and return the whois data.
     *
     * @return array Whois Data
     */
    public function clean(){
        //
        // Designed for Team Cymru
        //

        // Sanity check to make sure this is a string.
        if(!is_string($this->data) && $this->data !== false){
            
            // We got something other than a string, be wary!
            return;
        }

        // break up arrays by \n
        $whois = explode("\n", $this->data);

        // $whois = ['first line, keys', 'results'];

        // split each variable.
        foreach($whois as $key => $value){

            $whois[$key] = explode("|", $value);

        }

        // $whois = [[key1, key2, key3], [result1, result2, result3]];

        // Create a new array.
        $response = array();

        foreach($whois[1] as $key => $value){
            // Lets get the other key
            $whoisKey = htmlspecialchars(trim($whois[0][$key]));

            // Lets Clean, and insert the new value with its key into the array.
            $response[$whoisKey] = htmlspecialchars(trim($value));
        }

        // $reponse = [[key1 => result1], [key2 => result2], ...];

        // Return our results
        return $response;

    }


}