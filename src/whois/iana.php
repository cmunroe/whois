<?php

namespace whois;

/**
 * A Whois Class Built specificall for whois.iana.org
 * 
 * $response = $whois->ip('192.168.1.1')->get()->clean();
 */
class iana extends core
{

    /**
     * Lookup IP.
     *
     * @param string $ip
     * @return this
     */
    public function ip(string $ip){

        $this->setServer("whois.iana.org");
        
        $this->setQuery($ip);

        return $this;
    }

    /**
     * Clean up our response, and package into an array.
     *
     * @return array
     */
    public function clean(){

        // explode the whois into an array by line
        $whois = explode("\n", $this->data);

        // Create the return array
        $response = array();

        // loop through lines.
        foreach($whois as $key => $value){

            if($value == null){
                // This line is nothing...
            }

            elseif(substr($value, 0, 1) === "#"){
                // This has a character that we don't care about, a comment.
            }

            elseif(substr($value, 0, 1) === "%"){
                // This line has a character we don't care about, a comment.
            }

            else{

                if(strpos($value, ':') !== false){
                    // We found an array definer.
                    $strKey = strstr($value, ':', true);
                    $strVal = substr($value, (strpos($value, ':') + 1));


                    // sanitize inputs!
                    $strKey = htmlspecialchars(trim($strKey));
                    $strVal = htmlspecialchars(trim($strVal));

                    // append to array
                    $response[$strKey] = $strVal;

                }

                else{

                    // we couldn't find an array key. Return as normal.

                    // apend to return.
                    $response[] = htmlspecialchars(trim($value));

                }
    
            }

        }

        return $response;

    }

}